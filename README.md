# pre-commit-hooks

This repository contains various (specific) hooks for use with the
[pre-commit](https://pre-commit.com) framework. Running them requires Python
version **>=3.9**.

#### `apache_license`

This hook can be used to add an Apache 2.0 License header to various source
files. A minimal pre-commit configuration may look like the following:

```
repos:
  - repo: https://gitlab.com/iam-cms/pre-commit-hooks
    rev: v0.7.0
    hooks:
      - id: apache-license
```

Using the `args` keyword, the default behaviour of the hook can be adapted. The
following options exist:

* `-a/--author`: The author to use in the license header.
* `-m/--mapping`: Overwrite existing or add additional file types to the
  default file/comment style mapping. For example, to use Jinja comment styles
  in HTML files, the following mapping can be used: `-m html jinja`.
