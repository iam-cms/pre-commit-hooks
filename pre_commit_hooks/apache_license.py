# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import glob
import itertools
import os
import re
import shutil
import tempfile
from datetime import datetime
from datetime import timezone

import click


LICENSE_NOTICE = "{comment_start}Copyright [0-9]{{4}} {author}"
LICENSE = """{comment_start}Copyright {year} {author}
{comment_middle}
{comment_middle}Licensed under the Apache License, Version 2.0 (the "License");
{comment_middle}you may not use this file except in compliance with the License.
{comment_middle}You may obtain a copy of the License at
{comment_middle}
{comment_middle}    http://www.apache.org/licenses/LICENSE-2.0
{comment_middle}
{comment_middle}Unless required by applicable law or agreed to in writing, software
{comment_middle}distributed under the License is distributed on an "AS IS" BASIS,
{comment_middle}WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
{comment_middle}See the License for the specific language governing permissions and
{comment_middle}limitations under the License.{comment_end}"""


COMMENT_STYLES = {
    "asterisk": {
        "comment_start": "/* ",
        "comment_middle": " * ",
        "comment_end": " */",
        "newlines": 1,
    },
    "hash": {
        "comment_start": "# ",
        "comment_middle": "# ",
        "comment_end": "",
        "newlines": 0,
    },
    "html": {
        "comment_start": "<!-- ",
        "comment_middle": "   - ",
        "comment_end": " -->",
        "newlines": 1,
    },
    "jinja": {
        "comment_start": "{# ",
        "comment_middle": " # ",
        "comment_end": " #}",
        "newlines": 1,
    },
}


FILE_TYPE_MAPPING = {
    "c": "asterisk",
    "cpp": "asterisk",
    "css": "asterisk",
    "h": "asterisk",
    "hpp": "asterisk",
    "html": "html",
    "js": "asterisk",
    "py": "hash",
    "scss": "asterisk",
    "sh": "hash",
    "vue": "html",
}


@click.command()
@click.argument("paths", type=click.Path(exists=True), nargs=-1)
@click.option(
    "-a",
    "--author",
    default="Karlsruhe Institute of Technology",
    help="The author to use in the license header.",
)
@click.option(
    "-m",
    "--mapping",
    nargs=2,
    multiple=True,
    help="Overwrite existing or add additional file types to the default file/comment"
    " style mapping.",
)
@click.option(
    "-d",
    "--dry-run",
    is_flag=True,
    help="Notify about missing license headers, but do not apply them.",
)
def main(paths, author, mapping, dry_run):
    """Check for Apache 2.0 license headers in one or multiple files.

    The given paths can be either single files and/or directories that will be searched
    recursively for suitable file types to apply the header on.
    """
    file_type_mapping = FILE_TYPE_MAPPING

    # Extend the existing file type mapping, if applicable.
    for file_type, style in mapping:
        file_type_mapping[file_type] = style

    # Collect all files to check for a license header.
    file_lists = []

    for path in paths:
        if os.path.isfile(path):
            file_lists.append([path])
        else:
            for file_type in file_type_mapping:
                file_lists.append(
                    glob.iglob(
                        os.path.join(path, "**", f"*.{file_type}"), recursive=True
                    )
                )

    # Check for missing license headers.
    for file in itertools.chain(*file_lists):
        found_missing_header = False

        # Ignore directories.
        if os.path.isdir(file):
            continue

        # Ignore files with non-matching extensions.
        _, file_type = os.path.splitext(file)
        file_type = file_type[1:] if len(file_type) > 0 else file_type

        if file_type not in file_type_mapping:
            continue

        comment_style = COMMENT_STYLES[file_type_mapping[file_type]]

        # Check the file for an existing header.
        with open(file, mode="r+", encoding="utf-8") as f:
            # Create the fitting license header for the current file.
            license_header = LICENSE.format(
                author=author,
                year=datetime.now(timezone.utc).year,
                comment_start=comment_style["comment_start"],
                comment_middle=comment_style["comment_middle"],
                comment_end=comment_style["comment_end"],
            )
            license_header = "\n".join(
                [line.rstrip() for line in license_header.split("\n")]
            )

            # Check the first line of the file using a regex because of the potentially
            # differing year. If it matches, continue with the rest of the license
            # header.
            first_line = f.readline()
            license_notice = re.compile(
                LICENSE_NOTICE.format(
                    author=author,
                    comment_start=re.escape(comment_style["comment_start"]),
                )
            )

            if not license_notice.match(first_line):
                found_missing_header = True
            else:
                for f_line, header_line in itertools.zip_longest(
                    f, license_header.split("\n")[1:]
                ):
                    # Finished checking the whole license header.
                    if header_line is None:
                        break

                    if not f_line == f"{header_line}\n":
                        found_missing_header = True
                        break

        if found_missing_header:
            if dry_run:
                click.echo(f"No license header found in '{file}'.")
            else:
                with open(file, encoding="utf-8") as f:
                    file_content = f.read().lstrip()

                # Create a new file in the same directory with the header and file
                # content, then replace the existing one.
                tmp_file = tempfile.NamedTemporaryFile(
                    mode="w", dir=os.path.dirname(file), delete=False
                )

                try:
                    tmp_file.write(f"{license_header}\n")

                    if file_content:
                        tmp_file.write(comment_style["newlines"] * "\n")
                        tmp_file.write(file_content)

                    tmp_file.close()

                    # Copy the metadata of the original file, if supported.
                    shutil.copystat(file, tmp_file.name)
                    os.replace(tmp_file.name, file)

                    click.echo(f"Applied license header to '{file}'.")
                except Exception as e:
                    click.secho(str(e), fg="red")

                    try:
                        os.remove(tmp_file.name)
                    except FileNotFoundError:
                        pass


if __name__ == "__main__":
    main()
