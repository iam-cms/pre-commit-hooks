# Release history

## 0.7.0 (2024-10-14)

* Dropped support for Python 3.8 and added support for Python 3.13.

## 0.6.0 (2024-02-08)

* Added different amounts of blank newlines after the license header for each
  comment style when using the `apache_license` hook.

## 0.5.0 (2024-02-06)

* Removed the exclude (`-x`/`--exclude`) option of the `apache_license` hook,
  as pre-commit already includes such an option.

## 0.4.0 (2023-10-04)

* Added support for Python 3.12.

## 0.3.0 (2023-09-09)

* Dropped support for Python 3.7.
* Fixed the `apache_license` hook not working on Windows.

## 0.2.0 (2023-06-12)

* Dropped support for Python 3.6 and added support for Python 3.11.

## 0.1.1 (2022-05-13)

* The original file metadata is now preserved when using the `apache_license`
  hook.

## 0.1.0 (2022-04-16)

* Initial release.
